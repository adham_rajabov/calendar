calendar widget
===============
this will calendar

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist adham/yii2-calendar "@dev"
```

or add

```
"adham/yii2-calendar": "@dev"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \adham\calendar\AutoloadExample::widget(); ?>```