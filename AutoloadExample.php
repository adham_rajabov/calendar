<?php

namespace adham\calendar;

class AutoloadExample extends \yii\base\Widget
{

	public $options = array();

    public function run()
    {
    	if (!isset($this->options['showDate'])) {
    		$this->options['showDate'] = false;
    	}

    	if (!isset($this->options['url'])) {
    		$this->options['url'] = '';
    	}

    	if (!isset($this->options['year'])) {
    		$this->options['year'] = isset($_GET['year'])?$_GET['year']:date('Y');
    	}

    	if (!isset($this->options['month'])) {
    		$this->options['month'] = isset($_GET['month'])?$_GET['month']:date('m');
    	}

    	if (!isset($this->options['day'])) {
    		$this->options['day'] = isset($_GET['day'])?$_GET['day']:date('d');
    	}

		return $this->render('@vendor/admin/yii2-calendar/index', [
			'options' => $this->options
		]);
    }
}
