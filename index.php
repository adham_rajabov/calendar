<div class="col-md-12 myCalendarWidget">

    <center>	

	    <div>
	    	<a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month='.$options['month'].'&year='.($options['year']-1); ?>" style="text-decoration: none;">
	        	<button type="button" class="btn btn-default"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;<?php echo $options['year']-1; ?></button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=1&year='.$options['year']; ?>" style="text-decoration: none;">
		        <button type="button" class="btn <?php echo ($options['month']==1)?'btn-info':'btn-default'; ?>">Январь</button>
        	</a>
        	<a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=2&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==2)?'btn-info':'btn-default'; ?>">Февраль</button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=3&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==3)?'btn-info':'btn-default'; ?>">Март</button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=4&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==4)?'btn-info':'btn-default'; ?>">Апрель</button>
        	</a>
        	<a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=5&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==5)?'btn-info':'btn-default'; ?>">Май</button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=6&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==6)?'btn-info':'btn-default'; ?>">Июнь</button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=7&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==7)?'btn-info':'btn-default'; ?>">Июль</button>
    		</a>
    		<a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=8&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==8)?'btn-info':'btn-default'; ?>">Август</button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=9&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==9)?'btn-info':'btn-default'; ?>">Сентябрь</button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=10&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==10)?'btn-info':'btn-default'; ?>">Октябрь</button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=11&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==11)?'btn-info':'btn-default'; ?>">Ноябрь</button>
	        </a>
	        <a href="<?php echo $options['url'].($options['showDate']?'&day='.$options['day']:'').'&month=12&year='.$options['year']; ?>" style="text-decoration: none;">
	        	<button type="button" class="btn <?php echo ($options['month']==12)?'btn-info':'btn-default'; ?>">Декабрь</button>
	        </a>
	        <a href="<?php echo $options['url'].(isset($options['showDate'])?'&day='.$options['day']:'').'&month='.$options['month'].'&year='.($options['year']+1); ?>" style="text-decoration: none;">
	        	<button type="button" class="btn btn-default"><?php echo $options['year']+1; ?>&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></button>
        	</a>
	    </div>

    </center>

	<div class="col-md-12" style="height:5px;"></div>

	<?php if ($options['showDate']) : ?>

		<?php $daysCount = cal_days_in_month(CAL_GREGORIAN, $options['month'], $options['year']); ?>

	    <center>
		    <div class="btn-group">
			    <?php for($i = 1; $i <= $daysCount; $i++) : ?>
			    	<a href="<?php echo $options['url'].'&day='.$i.'&month='.$options['month'].'&year='.$options['year']; ?>" style="text-decoration: none;">
						<button type="button" class="btn <?php echo ($options['day']==$i)?'btn-info':'btn-default'; ?> btn-sm">
							<?php echo $i; ?>
						</button>
					</a>
			    <?php endfor; ?>
			</div>
		</center>

	<?php endif; ?>

</div>